import { createServer, logger, defaultConfig } from "../src";
import { expect } from "chai";
import { describe } from "mocha";
import { ImprovedError } from "@toes/core";
import { Request, Response, NextFunction } from "express";
import axios, { AxiosError } from "axios";

process.env.PROXY_BYRD_CONFIG = `test/config.yaml`;
process.env.DEBUG = "true";
logger.debug(`Using config file: ${process.env.PROXY_BYRD_CONFIG}`);

describe("createServer()", () => {
    const { app, httpServer } = createServer(defaultConfig());
    app.use(
        (
            err: ImprovedError,
            req: Request,
            _res: Response,
            next: NextFunction
        ) => {
            if (err)
                if (process.env.DEBUG === "true")
                    logger.error(`[${req.method}] ${err.code}: ${err.message}`);
                else next();
        }
    );
    const client = axios.create({ baseURL: "http://localhost:8888" });

    it("index", (done) => {
        client
            .get("/")
            .then((res) => {
                expect(res.status).to.eq(200);
                expect(res.data).to.include("Hello World");
                done();
            })
            .catch((err) => {
                done(err);
            });
    });

    it("404", (done) => {
        client
            .get("/error1")
            .then(() => {
                httpServer.close();

                done();
            })
            .catch((err: AxiosError) => {
                const res = err.response;
                expect(res.status).to.eq(404);
                expect(res.data.error).to.include("Not Found");
                httpServer.close();
                done();
            });
    });
});
