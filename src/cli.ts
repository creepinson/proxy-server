import { NextFunction, Request, Response } from "express";
import { createServer } from ".";
import { defaultConfig } from "./config";
import { ImprovedError } from "@toes/core";
import yargs from "yargs";
import { hideBin } from "yargs/helpers";
import { logger } from "./config";
const argv = yargs(hideBin(process.argv))
    .option("config", {
        alias: "c",
        type: "string",
        description: "path to configuration file",
    })
    .option("debug", {
        alias: "d",
        type: "boolean",
        description: "Enable debug logging (default = 'false')",
        default: "false",
    }).argv;
if (argv.debug) process.env.DEBUG = argv.debug;
if (argv.config) {
    process.env.PROXY_BYRD_CONFIG = argv.config as string;
    if (process.env.DEBUG === "true")
        logger.debug(`Using config file: ${process.env.PROXY_BYRD_CONFIG}`);
}
const { app } = createServer(defaultConfig());
app.use(
    (err: ImprovedError, req: Request, res: Response, next: NextFunction) => {
        if (err) {
            if (process.env.DEBUG === "true")
                logger.debug(`[${req.method}] ${err.code}: ${err.stack}`);
            else logger.debug(`[${req.method}] ${err.code}: ${err.message} 🍔`);
            return res.redirect(`https://http.cat/${err.code ?? 500}`);
        } else return next();
    }
);
